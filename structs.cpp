#include<iostream>
#include<cstring>
using namespace std;

enum Gender {MALE, FEMALE, TUMTUM, ANDROGENUS};

struct Date {
	int day;
	int month;
	int year;
};

struct Person {
	int id; // 4
	char name[20]; // 20
	Gender gender; // 4
	bool vaccinated; // 1 (4)
	Date birthday; // 12
	double age; // 8
	// Person* father = nullptr;
};

int main(void) {
	Person p1;
	cout << "p1: " << sizeof p1 << endl;
	cout << "p1.id: " << sizeof p1.id << endl;
	cout << "p1.name: " << sizeof p1.name << " " << (char*)&(p1.name) - (char*)&(p1) << endl;
	cout << "p1.gender: " << sizeof p1.gender << " " << (char*)&(p1.gender) - (char*)&(p1) << endl;
	cout << "p1.vaccinated: " << sizeof p1.vaccinated << " " << (char*)&(p1.vaccinated) - (char*)&(p1) << endl;
	cout << "p1.birthday: " << sizeof p1.birthday << " " << (char*)&(p1.birthday) - (char*)&(p1) << endl;
	cout << "p1.age: " << sizeof p1.age << " " << (char*)&(p1.age) - (char*)&(p1) << endl;

	p1.id = 123456789;
	strncpy(p1.name,"Yossi Cohen", 19);
	p1.gender = MALE;
	p1.birthday.day = 13;
	p1.birthday.month = 2;
	p1.birthday.year = 2001;
	p1.age = 20.2;
	//p1.father = &p1;
	//Person p2, p3;
	//delete p1;
	return 0;
}


