/*
 * program.cpp
 *
 *  Created on: 12 Nov 2021
 *      Author: dzilb
 */
#include <iostream>
#include <algorithm>

using namespace std;

void sortInsertion(int nums[], int first, int last);
void sortBuble(int nums[], int first, int last);
void printArray(int*, int*);
int linearSearch(int[], int, int, int);
/* find a value in an array in elements between first and
 * last (including).
 * return the index of found element
 * if not found - return -1
 */
int binarySearch(int[], int, int, int);
/* find a value in a SORTED array in elements between
 * first and last (including).
 * return the index of found element
 * if not found - return -1
 */

void merge(int[], int, int, int[], int, int, int[], int);

int mainSearchSort() {
	int arr[] = { 4, 9, 2, 5, 8, 1, 7 };
	int size = sizeof(arr) / sizeof(int);
	int sortedArr[] = { 1, 2, 4, 5, 7, 8, 9 };

	printArray(arr, arr + size);
	//sortInsertion(arr, 0, size - 1);
	//sortBuble(arr, 0, size - 1);
	sort(arr, arr + size);
	printArray(arr, arr + size);
	printArray(sortedArr, sortedArr + size);

//	for (auto &item : arr)
//		++item;
//
//	printArray(arr, arr + size);
//
//	cout << LinearSearch(arr, 0, size - 1, 8) << endl;
//	cout << LinearSearch(arr, 0, size - 1, 6) << endl;
//
//	cout << BinarySearch(sortedArr, 0, size - 1, 8) << endl;
//	cout << BinarySearch(sortedArr, 0, size - 1, 1) << endl;
//	cout << BinarySearch(sortedArr, 0, size - 1, 4) << endl;
//	cout << LinearSearch(sortedArr, 0, size - 1, 6) << endl;

//	int sorted1[] = { 1, 4, 7, 10, 15, 18 };
//	int sorted2[] = { 3, 6, 8, 10, 12, 17, 19 };
//	int size1 = sizeof(sorted1) / sizeof(int);
//	int size2 = sizeof(sorted2) / sizeof(int);
//	int size = size1 + size2;
//	int *sorted = new int[size];
//	merge(sorted1, 0, size1 - 1, sorted2, 0, size2 - 1, sorted, 0);
//	printArray(sorted1, sorted1 + size1);
//	printArray(sorted2, sorted2 + size2);
//	printArray(sorted, sorted + size);

	return 0;
}

void printArray(int *begin, int *end) {
	while (begin < end)
		cout << *(begin++) << " ";
	cout << endl;
}

int linearSearch(int numbers[], int first, int last, int value) {
	for (int i = first; i <= last; ++i)
		if (value == numbers[i])
			return i;
	return -1;
}

int binarySearch(int numbers[], int first, int last, int value) {
	if (value < numbers[first] || value > numbers[last])
		return -1;

	while (first <= last) {
		int mid = (first + last) / 2;
		if (value == numbers[mid])
			return mid; // found
		if (value < numbers[mid])
			last = mid - 1; // go to the left
		else
			first = mid + 1; // go to the right
	}

	// not found
	return -1;
}

void sortInsertion(int nums[], int first, int last) {
	for (int i = first + 1; i <= last; ++i) {
		int temp = nums[i];
		int j;
		for (j = i - 1; j >= 0 && temp < nums[j]; --j)
			nums[j + 1] = nums[j];
		nums[j + 1] = temp;
	}
}

void swap(int nums[], int index1, int index2) {
	int temp = nums[index1];
	nums[index1] = nums[index2];
	nums[index2] = temp;
}

void sortBuble(int nums[], int first, int last) {
	bool swapped = true;
	for (int i = last - 1; swapped && i >= 0; --i) {
		swapped = false;
		for (int j = 0; j <= i; ++j) {
			if (nums[j] > nums[j + 1]) {
				swap(nums, j, j + 1);
				swapped = true;
			}
		}
	}
}

void merge(int from1[], int start1, int end1, int from2[], int start2, int end2,
		int to[], int start) {
	int size = end1 - start1 + 1 + end2 - start2 + 1;
	for (int i = 0; i < size; ++i) {
		if (start2 > end2)
			to[i] = from1[start1++];
		else if (start1 > end1)
			to[i] = from2[start2++];
		else if (from1[start1] == from2[start2]) {
			to[i] = from1[start1++];
			start2++;
			size--;
		} else if (from1[start1] < from2[start2])
					to[i] = from1[start1++];
		else
			to[i] = from2[start2++];
	}
}
