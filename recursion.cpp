#include <iostream>
using namespace std;

void moveOneRing(int from, int to) {
	static int count = 0;
	cout << ++count << ": " << from << " to " << to << endl;
}

void moveTower(int from, int to, int n) {
	if (n == 1) {
		moveOneRing(from, to);
		return;
	}
	int help = 3 - from - to;
	moveTower(from, help, n - 1);
	moveOneRing(from, to);
	moveTower(help, to, n - 1);
}

int binarySearchRec(int arr[], int from, int to, int value) {
	if (from > to) return -1;

	int mid = (from + to) / 2;
	if (arr[mid] == value) return mid;

	if (arr[mid] < value)
		return binarySearchRec(arr, mid + 1, to, value);
	else
		return binarySearchRec(arr, from, mid - 1, value);
}

int arithSequence(int n, int a0, int d) {
	if (n == 0) return a0;
	return arithSequence(n - 1, a0, d) + d;
}

int fibonacci(int n) {
	if (n <= 1) return n;
	return fibonacci(n - 2) + fibonacci(n - 1);
}

int factorial(int n) {
	if (n == 0) return 1; // 0! = 1
	return n * factorial(n - 1); // n! = (n-1)! * n
}

void printDigits1(int n) {
	if (n < 10) cout << n << endl;
	else {
		cout << n % 10 << endl;
		printDigits1(n / 10);
	}
}

void printDigits2(int n) {
	if (n < 10) cout << n << endl;
	else {
		printDigits2(n / 10);
		cout << n % 10 << endl;
	}
}

int mult(int a, int b) {
	if (b == 1) return a;
	return mult(a, b - 1) + a;
}

void merge(int from1[], int start1, int end1, int from2[], int start2, int end2,
		int to[], int start);

void sortMerge(int arr[], int start, int end) {
	if (end <= start) return;

	int size = end - start + 1;
	int* temp = new int[size];
	for (int i = start, j = 0; i <= end; ++i, ++j )
		temp[j] = arr[i];

	int mid = (start + end) / 2;

	sortMerge(temp, 0, mid);
	sortMerge(temp, mid + 1, size - 1);

	merge(temp, 0, mid, temp, mid + 1, size - 1, arr, start);

	delete[] temp;
}

void sortMerge(int arr[], int temp[], int from, int to) {
    if (to <= from) return;

	int mid = (from + to) / 2;
    sortMerge(arr, temp, from, mid);
    sortMerge(arr, temp, mid + 1, to);

    for (int i = from; i <= to ; ++i)
    	temp[i] = arr[i];

    merge(temp, from, mid, temp, mid + 1, to, arr, from);
}

void sortMerge(int arr[], int size) {
	int* temp = new int[size];
	sortMerge(arr, temp, 0, size - 1);
	delete [] temp;
}

int mainRecursion() {
	//moveTower(0, 1, 3);
	printDigits1(1234567);
	cout << endl;
	printDigits2(1234567);
	cout << endl;
	return 0;
}

